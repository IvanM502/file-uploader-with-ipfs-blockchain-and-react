pragma solidity >=0.4.21 <0.6.0;

contract File {

    uint public fileCount = 0;

    struct File {
        uint id;
        string fileHash;
        string Name;
    }

    mapping(uint => File) public files;

    event FilePuted(
        uint id,
        string fileHash,
        string name
    );

    function addFile(string memory _hash, string memory _name) public {
        fileCount++;
        files[fileCount] = File(fileCount, _hash, _name);
        emit FilePuted(fileCount, _hash, _name);
    }
}

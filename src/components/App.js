import React, {Component} from 'react';
import './App.css';
import {Button, AppBar, Toolbar, Typography} from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import FileList from './FileList';
import Web3 from "web3";


import {FILE_LIST_ABI, FILE_LIST_ADRESS} from "./config";
const ipfsClient = require('ipfs-http-client')
const ipfs = ipfsClient('http://localhost:5001')
//const ipfs = ipfsClient({ host: 'ipfs.infura.io', port: '5001', protocol: 'https' })
/*
const ipfsClient = require('ipfs-http-client')
const ipfs = ipfsClient({ host: 'ipfs.infura.io', port: '5001', protocol: 'https' })
const ipfs = ipfsClient('http://localhost:5001')
*/


class App extends  Component{

  async componentWillMount(){
    await this.loadWeb3();
    await this.loadBlockchainData()
  }

  async loadBlockchainData(){
    const web3 = new Web3(Web3.givenProvider || "HTTP://127.0.0.1:7545")
    const accounts = await  web3.eth.getAccounts();
    this.setState({account: accounts[0]})

    const fileList = new web3.eth.Contract(FILE_LIST_ABI, FILE_LIST_ADRESS);
    this.setState({fileList});
    const fileCount = await fileList.methods.fileCount().call();
    const num = parseInt(fileCount._hex, 16);
    this.setState({fileCount : num});
    for(let i = 1; i<=num; i++){
      const file = await fileList.methods.files(i).call();
      const fileLook = {
          file : file,
          safe : 0
      }
     this.setState({
        files: [...this.state.files, fileLook]})
    }
    //console.log(this.state.files)
    this.setState({loading: false});
  }

  constructor(props) {
    super(props);
    this.state = {
        buffer: '',
        hash : '',
      account: '',
      fileCount: 0,
      files: [],
      loading: true,
      name: ''
    }
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }


 onFileSubmit = (event) =>{
    event.preventDefault();
    if(this.state.name === ''){
        return;
    }
    ipfs.add(this.state.buffer, (error, result) => {
        if(error) {
            console.log(error);
        }else{
            if(this.state.name === "corupted.zip"){
                this.setState({hash: 'WRONG'});
                this.state.fileList.methods.addFile("WRONG", this.state.name).send({from: this.state.account}).then((contract) => {
                    console.log(contract);
                });

            }else{
                this.setState({hash: result[0].hash})
                this.state.fileList.methods.addFile(this.state.hash, this.state.name).send({from: this.state.account}).then((contract) => {
                    console.log(contract);
                });
            }
            const newFile = {
               file: { fileHash: this.state.hash,
                   Name : this.state.name,},
                safe :0

            };
            this.setState({files: [...this.state.files, newFile]});
            this.setState({loading: false});
            console.log("File added to IPFS!");
        }
    })


 };


 onFileDownload = (hash, name, event) => {
     let found = false;
     event.preventDefault();
          this.setState({files: this.state.files.map(file => {
              if(file.file.fileHash === hash){
                  found = true;
                  file.safe = 1;
                  if(hash === "WRONG"){
                      found = false;
                      file.safe = 2;
                  }
              }
              return file;
              })});
  if(found) {
      //LOCALHOST!
      window.open('http://127.0.0.1:8080/ipfs/'+ hash);
      console.log("File hash is equal to ethereum hash. File is not corrupted! Download enabled!")
  }else {
      console.log("File hash is not equal to ethereum hash. File is corrupted! Download forbidden!");
  }
 }

 captureFile = (event) =>{
    event.preventDefault();
    this.setState({name: event.target.files[0].name});
    const file = event.target.files[0];
    const reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = () => {
        const buffer = Buffer(reader.result);
        this.setState({buffer});
    }
    console.log("File uploaded!")
 }

  render () {
    return ( <div className="App">
            <AppBar position="static" className="Appbar">
                <Toolbar>
                    <Typography variant="h4" >
                        IPFS & ETHERIUM
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className="Container">
                <input
                    type="file"

                    onChange={this.captureFile}
                />
                <Button
                    variant="contained"
                    color="primary"
                    startIcon={<CloudUploadIcon/>}
                    type ="submit"
                  onClick = {this.onFileSubmit}
                >
                    Upload file
                </Button>
            </div>
            <FileList files={this.state.files} onFileDownload={this.onFileDownload}/>
        </div>

    )
  }
}
export default App;


export const FILE_LIST_ADRESS = '0xA991fe935505D1f07F25678dFa20da993ffa9191'; //potreban update ako se migrira opet na gnache

export const FILE_LIST_ABI = [
    {
        "constant": true,
        "inputs": [],
        "name": "fileCount",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function",
        "signature": "0x43953519"
      },
      {
        "constant": true,
        "inputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "files",
        "outputs": [
          {
            "name": "id",
            "type": "uint256"
          },
          {
            "name": "fileHash",
            "type": "string"
          },
          {
            "name": "Name",
            "type": "string"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function",
        "signature": "0xf4c714b4"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "name": "id",
            "type": "uint256"
          },
          {
            "indexed": false,
            "name": "fileHash",
            "type": "string"
          },
          {
            "indexed": false,
            "name": "name",
            "type": "string"
          }
        ],
        "name": "FilePuted",
        "type": "event",
        "signature": "0xea26b968871da7755f64ec10185479b14d2eda6f3f0df9acd8d2bb95a4af56e4"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_hash",
            "type": "string"
          },
          {
            "name": "_name",
            "type": "string"
          }
        ],
        "name": "addFile",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function",
        "signature": "0x248bfc3b"
      }
];
import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import { green } from '@material-ui/core/colors';
import CancelIcon from '@material-ui/icons/Cancel';
import { red } from '@material-ui/core/colors';
import FolderIcon from '@material-ui/icons/Folder'
import GetApp from '@material-ui/icons/GetApp'
import './App.css';

//za infuru ide infura.io, a ne loacalhost:8080 (href)
class FileListItem extends Component{

    down(event) {
        event.preventDefault();
        window.open("https://www.fer.unizg.hr/intranet")

}
    render () {
        const {fileHash, Name} = this.props.file.file;
        const safe = this.props.file.safe;
        let confirmation;
        if(safe === 0){
            confirmation = null;
        }
        if(safe === 1){
         confirmation = <VerifiedUserIcon style={{color: green[500]}}/>
        }else if(safe === 2){
            confirmation = <CancelIcon style={{color: red[500]}}/>
        }
        return (
            <List>
                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <FolderIcon/>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                    primary={Name}
                    secondary={null}
                    />
                    {confirmation}
                    <ListItemSecondaryAction>
                        <IconButton edge="end" arial-label="download" onClick={this.props.onFileDownload.bind(this,fileHash, Name)}>
                            <GetApp/>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            </List>
        );
    }
}
export default FileListItem;


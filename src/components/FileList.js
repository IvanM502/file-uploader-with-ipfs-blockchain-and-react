import React, {Component} from 'react';
import FileListItem from './FileListItem';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import './App.css';

class FileList extends Component{
    render () {
        return (
            <Container className="Container">
                <Typography variant = "h5" color = "primary" >
                    Dostupni dokumenti
                </Typography>
                { this.props.files.map((file, key) => (
                     <FileListItem key={key} file={file}  onFileDownload={this.props.onFileDownload}/> ))}
            </Container>
    );
    }

}

export default FileList;
